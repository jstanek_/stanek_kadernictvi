<article>
            <h1>Náš ceník</h1>
        <div>
            <div class="panske">
                <h2>Pánské</h2>
                <p> Pánský střih strojkem - 150 Kč </p>
                <p> Úprava vousů (kníru)  - 80  Kč </p>
                <p> Úprava vousů (celé)   - 120 Kč </p>
            </div>
            
            <div class="damske">
                <h2>Dámské</h2>
                <p> Dámský střih (krátké vlasy) - 500 Kč </p>
                <p> Dámský střih (dlouhé vlasy)  - 800  Kč </p>
                <p> Přeliv - 720 Kč </p>
                <p>Barvení - 900 Kč</p>
                <p>Melír - 1500 Kč</p>
                <p>Svatební účes - 1800 Kč</p>
                <p>Ombré - 2200 Kč</p>
            </div>
            
            <div class="detske">
                <h2>Dětské</h2>
                <p> Dětský střih (do 5 let) - 80 Kč </p>
                <p> Dětský střih (do 10 let)  - 120  Kč </p>
            
            </div>
           </div>             
    </article>