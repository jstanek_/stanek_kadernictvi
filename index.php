<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>Kadeřnictví Locika</title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <link rel="stylesheet" href="styles/style/style.css" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
  </head>
  <body>
    <?php require "inc/index_header.php";?>
    <?php include "inc/index_article.php";?>
    <?php include "inc/index_div_duvody.php";?>
    <?php include "inc/index_div_ukazka.php";?>
    <?php include "inc/index_footer.php";?>                           
  </body>
</html>