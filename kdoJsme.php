<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>Kadeřnictví Locika - Kdo jsme</title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <link rel="stylesheet" href="styles/kdoJsme/kdoJsme.css" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
  </head>
  <body>
    <?php require "inc/kdoJsme_header.php";?>
    <?php include "inc/kdoJsme_article.php";?>
    <?php include "inc/kdoJsme_div.php";?>
    <?php include "inc/kdoJsme_footer.php";?> 

  </body>
</html>