<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>Kadeřnictví Locika - Kontakty</title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <link rel="stylesheet" href="styles/kontakty/kontakty.css" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
  </head>
  <body>
        <?php require "inc/kontakty_header.php";?>
        <?php include "inc/kontakty_article.php";?>
        <?php include "inc/kontakty_div_map.php";?>
        <?php include "inc/kontakty_div_ig.php";?>
        <?php include "inc/kontakty_iframe.php";?>
        <?php include "inc/kontakty_footer.php";?>
              

  </body>
</html>