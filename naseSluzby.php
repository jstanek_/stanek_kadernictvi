<!DOCTYPE html>
<html lang='cs'>
  <head>
    <title>Kadeřnictví Locika - Naše služby</title>
    <meta charset='utf-8'>
    <meta name='description' content=''>
    <meta name='keywords' content=''>
    <meta name='author' content=''>
    <meta name='robots' content='all'>
    <!-- <meta http-equiv='X-UA-Compatible' content='IE=edge'> -->
    <link href='/favicon.png' rel='shortcut icon' type='image/png'>
    <link rel="stylesheet" href="styles\naseSluzby\naseSluzby.css" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
  </head>
  <body>
        <?php require "inc/naseSluzby_header.php";?>
        <?php include "inc/naseSluzby_article.php";?>
        <?php include "inc/naseSluzby_footer.php";?>
  </body>
</html>